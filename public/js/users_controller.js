var UsersController = angular.module('UsersController',['ui.bootstrap']);

UsersController.controller('UsersController', function($scope, $modal, $log, $http) {

	$http.get('/users').success(function(data, status, headers, config) {
		$scope.users = data.users;
		$scope.predicate = 'name';
    }).error(function(data, status, headers, config) {
		console.log(data, status, headers, config);
    });
    
	$scope.pageTitle = "MEAN CRUD"

	$scope.usersTableisVisible = true;
	$scope.usersTableToggleButtonTitle = "Hide Users Table";
	$scope.toggleUsersTable = function() {
		// $scope.usersTableisVisible = !$scope.usersTableisVisible;
		if($scope.usersTableisVisible) {
			$scope.usersTableToggleButtonTitle = "Show Users Table";
			$scope.usersTableisVisible = false;
		}
		else {
			$scope.usersTableToggleButtonTitle = "Hide Users Table";
			$scope.usersTableisVisible = true;
		}
	}

	$scope.addUser = function() {

		var new_user = {name:$scope.name, email:$scope.email, password:$scope.password};
		$http.post('/users', new_user).success(function(data, status, headers, config) {
			console.log(data, status, headers, config);
			$http.get('/users').success(function(response) {
				$scope.users = response.users;
			});
	    }).error(function(data, status, headers, config) {
			console.log(data, status, headers, config);
	    });
	}

	$scope.removeUser = function(index, id) {
		$http.delete('/users/' + id).success(function(data, status, headers, config) {
			$scope.users.splice(index, 1);
		});
	}

	$scope.openEditUser = function (user, id) {
		$scope.user = user;
		// Remove password from input
		$scope.user.password = '';
		console.log('Editing: ' + id);
		$modal.open({
			templateUrl: 'editModalContent.html',
			backdrop: true,
			windowClass: 'modal',
			controller: function($scope, $modalInstance, user) {
				$scope.user = user;
				$scope.save = function () {
					$http.put('/users/' + id, {name:user.name, email:user.email, password:user.password}).success(function(data, status, headers, config) {
						console.log(data);
						$modalInstance.close();					
					}).error(function(data, status, headers, config) {
						console.log(data, status);
					});

				},
				$scope.cancel = function () {
					$modalInstance.dismiss();
				}
			},
			resolve: {
				user: function () {
					return $scope.user;
				}
			}
		});
	};
	
	$scope.alerts = [
		{ type: 'danger', msg: 'Oh snap! Change a few things up and try submitting again.' },
		{ type: 'success', msg: 'Well done! You successfully read this important alert message.' }
	];

	$scope.addAlert = function() {
		$scope.alerts.push({msg: "Another alert!"});
	};

	$scope.closeAlert = function(index) {
		$scope.alerts.splice(index, 1);
	};	

});